import { Button, Row, Col } from "react-bootstrap";

import { Link } from "react-router-dom";

export default function Banner() {
  return (
    <Row>
      <Col className="p-5">
        <h1>You have our full attention!</h1>
        <p>Right away sir!</p>
        <Button as={Link} to="/courses" variant="primary">
          Enroll now!
        </Button>
      </Col>
    </Row>
  );
}
