import React from "react";

// Create a Context Object
// context object-is a data type of an object that can be used to store info that can be shared within the app
// context obj is a diff approach to passing info between components and allows easier access avoiding the use of props

// using the createContext from react we can create a context in our app
// we contained the context created in our UserContext variable
// we named it userContext simply bec this will contain the info of our user
const UserContext = React.createContext();

// the provider component allows other components to consume/use the context obj and supply the necessary info needed to the context object.
export const UserProvider = UserContext.Provider;
export default UserContext;
